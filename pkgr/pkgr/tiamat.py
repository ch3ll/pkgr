# Import python libs
import os
import shutil
import subprocess
import sys


def __virtual__(hub):
    return all(shutil.which(cmd) for cmd in ("tar", "tiamat"))


def build(hub):
    """
    Build the package using tiamat
    """
    # Copy the required files into salt dir
    # build with tiamat with build.conf file
    sources = os.path.join(hub.pkgr.SDIR, "salt")
    for fn in os.listdir(hub.OPT.pkgr.sources):
        full = os.path.join(hub.OPT.pkgr.sources, fn)
        shutil.copy(full, sources)

    os.chdir(sources)
    subprocess.run("tiamat build --config build.conf", shell=True)

    # create versioned tarfile
    os.chdir("dist")
    tcmd = f"tar -czvf salt-{hub.pkgr.VER}.tar.gz *"
    subprocess.run(tcmd, shell=True)
    artifacts = os.path.join(hub.pkgr.CDIR, "artifacts")
    if not os.path.isdir(artifacts):
        print(f"[pkgr] Making Directory: {artifacts}")
        os.mkdir(artifacts)
    shutil.copy(f"salt-{hub.pkgr.VER}.tar.gz", artifacts)
    sys.stdout.flush()


def render(hub):
    pass
